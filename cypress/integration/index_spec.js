describe('Oi Callcenter home', function() {
    it('Acessando a página inicial', function() {
        cy.visit('/');
        cy.contains('Oi Call Center');
    });

    it('Tentando acessar sem e-mail', function() {
        cy.contains('Entrar').click();
        cy.get('.btn-submit').click();

        cy.get('.input-group-invalid').should('be.visible');
    });

    it('Inserindo e-mail e senha', function() {
        cy.get('#id_username').type('igor');
        cy.get('#id_password').type('OiCall123');
        cy.contains('Entrar').click();

        cy.contains('Insira abaixo o MSISDN que deseja fazer uma consulta ou adesão');
        cy.url().should('include', '/busca-msisdn');
    });
});

describe('Busca de msisdn', function() {
    beforeEach(function () {
        cy.request('/login/').its('body').then((body) => {
            const csrf = Cypress.$(body).find("input[name=csrfmiddlewaretoken]").val();
            cy.request({
                method: 'POST',
                url: '/login',
                failOnStatusCode: false,  dont fail so we can make assertions
                form: true,  we are submitting a regular form body
                body: {
                username: "igor",
                password: "OiCall123",
                csrfmiddlewaretoken: csrf
                } 
            });
        });
        cy.visit('/');
        cy.get('#id_username').type('igor');
        cy.get('#id_password').type('OiCall123');
        cy.contains('Entrar').click();
    });

    it('não cadastrado', function() {
        cy.get('#id_msisdn').type('21980000000');
        cy.contains('Buscar MSISDN').click();
        cy.get('#id_msisdn').should('have.value', '(21)98000-0000')

        cy.contains('Adesão Oi Controle Sem Fatura');
    });

    it('cadastrado como cartão', function() {
        cy.get('#id_msisdn').type('(21)987458745');
        cy.contains('Buscar MSISDN').click();

        cy.contains('Alterar cartão');
    });

    it('cadastrado como boleto', function() {
        cy.get('#id_msisdn').type('(21)987456321');
        cy.contains('Buscar MSISDN').click();

        cy.contains('BOLETO DIGITAL');
    });
});